﻿using UnityEngine;
using System.Collections;
using System.Linq;


[RequireComponent(typeof(RotationModule))]
public class StateChasePlayer : MonoBehaviour, IState
{
	public Transform target;
	public float speed;
	BirdFunc bird;
	private RotationModule rotationModule;

	void Start()
	{
		bird = GetComponent<BirdFunc>();
		rotationModule = GetComponent<RotationModule>();
	}


	public float playerMaxDistance = 100f;
	public AnimationCurve priorityPlayerDist, priorityPlayerHealth;
	public float mulPlayerDist, mulPlayerHealth;

	public string GetName()
	{
		return "Chase Player";
	}

	public bool GetUninterruptible()
	{
		return false;
	}

	public float GetPriority()
	{
		var playerDist = priorityPlayerDist.Evaluate((target.position - transform.position).magnitude / playerMaxDistance) * mulPlayerDist;
		var playerHealth = priorityPlayerHealth.Evaluate(FuckCameraWhenNotSafe.instance.GetHealth()) * mulPlayerHealth;

		return playerDist * playerHealth;

	}

	public void OnEnter()
	{
		//AudioNodesManager.PostEvent("Chase", gameObject);
	}

	public float distanceToWin = 4f;

	public void OnExecute(float deltaTime)
	{

		var flyDir = target.position - transform.position;
		var dist = flyDir.magnitude;
		flyDir /= dist;
		flyDir *= speed;
		Debug.DrawRay(transform.position + Vector3.up * 3, flyDir * 3, Color.green);

		var lookdir = flyDir;
		lookdir.y = 0;

		rotationModule.RotateTowards(lookdir);

		// fly in the sky
		transform.position += flyDir * deltaTime;

		bird.FlyDown(deltaTime);


		// if player is closeby, win
		if (dist < distanceToWin) {
			WinConditions.instance.Win();
		}
	}

	public void OnExit()
	{

	}

	/// <summary>
	/// idle is always an option
	/// </summary>
	/// <returns></returns>
	public bool ConditionsMet()
	{
		return true;
	}
}
