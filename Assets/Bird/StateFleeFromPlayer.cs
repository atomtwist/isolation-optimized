﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BirdFunc))]
[RequireComponent(typeof(RotationModule))]
public class StateFleeFromPlayer : MonoBehaviour, IState
{


	public AnimationCurve priorityPlayerDist, priorityPlayerHealth;
	public float mulPlayerDist, mulPlayerHealth;

	public float distanceToFlee = 10;
	public Transform player;
	public float fleeSpeed = 1;

	public float uninterruptibleTime = 3f;
	float uninterruptibleTimer = 0;

	BirdFunc bird;
	private RotationModule rotationModule;

	void Start()
	{
		bird = GetComponent<BirdFunc>();
		rotationModule = GetComponent<RotationModule>();
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	public string GetName()
	{
		return "Flee from player";
	}

	public bool GetUninterruptible()
	{
		return uninterruptibleTimer > 0;
	}

	public float GetPriority()
	{
		var playerDist = priorityPlayerDist.Evaluate((player.position - transform.position).magnitude/distanceToFlee) * mulPlayerDist;
		var playerHealth = priorityPlayerHealth.Evaluate(FuckCameraWhenNotSafe.instance.GetHealth()) * mulPlayerHealth;

		return playerDist * playerHealth;

	}

	public void OnEnter()
	{
		StartCoroutine(pTween.To(uninterruptibleTime, 1, 0, t => {
			uninterruptibleTimer = t;
		}));
	}

	public void OnExecute(float deltaTime)
	{
		// fle from player
		var playerTowardsTransform = transform.position - player.position;
		rotationModule.RotateTowards(playerTowardsTransform.normalized);
		transform.position += playerTowardsTransform.normalized * deltaTime * fleeSpeed;

		bird.FlyDown(deltaTime);
	}


	public void OnExit()
	{
	}

	public bool ConditionsMet()
	{
		return true;
	}
}
