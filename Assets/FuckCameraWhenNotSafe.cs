﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class FuckCameraWhenNotSafe : MonoBehaviour
{
	PotatoCounter potatoCounter;
	public static FuckCameraWhenNotSafe instance;

	PlayerSafe playerSafe;
	public float unsafeTime;

	public Camera[] cams;

	public AnimationCurve vignette, blurEdges, blurDistance, chromaticAbberation, overlay, overlay2, overlay3;

	public float cycleDurationMultiplier;
	public float potatoPickupSafetyAmount = 5;
	public float potatoPickupSafetyAnimDuration = 0.5f;

	public bool executeInEditMode;

	void Awake()
	{
		instance = this;

	}

	public float GetHealth()
	{
		return 1f - unsafeTime / maxUnsafeTime;
	}

	// Use this for initialization
	void Start()
	{
		playerSafe = GetComponent<PlayerSafe>();
		potatoCounter = transform.parent.GetComponentInChildren<PotatoCounter>();

	}

	void Update()
	{
		if ((executeInEditMode && Application.isEditor && !Application.isPlaying) || Application.isPlaying) {
			UpdateUnsafeTime();
			UpdateCameraEffects();
		}
	}

	void UpdateCameraEffects()
	{
		foreach (var c in cams) {
			var vca = c.GetComponent<VignetteAndChromaticAberration>();
			var effect = vignette.Evaluate(unsafeTime);
			vca.intensity = effect;
			effect = blurEdges.Evaluate(unsafeTime);
			vca.blur = effect;
			effect = blurDistance.Evaluate(unsafeTime);
			vca.blurDistance = effect;
			effect = chromaticAbberation.Evaluate(unsafeTime);
			vca.chromaticAberration = effect;
			c.GetComponents<ScreenOverlay>()[1].intensity = overlay.Evaluate(unsafeTime);
			c.GetComponents<ScreenOverlay>()[0].intensity = overlay2.Evaluate(unsafeTime);
			c.GetComponents<ScreenOverlay>()[2].intensity = overlay3.Evaluate(unsafeTime);
		}
	}

	public float coolDownMultiplier;
	public float maxUnsafeTime = 30f;

	bool chaseOn = false;

	public float timeSinceDead = 0;
	public float timeToCreateEnemies = 5f;
	public Transform enemyPrefab;
	public Vector3 positionOffset;
	public AnimationCurve sidewaysDistribution;
	public float raiseUpTime, raiseUpSpeed;

	public List<Transform> enemiesToKill = new List<Transform>();

	void UpdateUnsafeTime()
	{
		if (unsafeTime <= 0) {
			unsafeTime = 0;
		}

		if (unsafeTime == maxUnsafeTime) {
			timeSinceDead += Time.deltaTime;

		} else {
			timeSinceDead = 0;
			// kill enemies
			foreach (var e in enemiesToKill) {
				// leave particle system to fly a bit and then kill that too
				var ps = e.GetComponentInChildren<ParticleSystem>();
				ps.transform.SetParent(null);
				ps.emissionRate = 0;
				Destroy(ps.gameObject, ps.duration);
				Destroy(e.gameObject);

			}
			enemiesToKill.Clear();
		}

		if (timeSinceDead > timeToCreateEnemies) {
			timeSinceDead -= timeToCreateEnemies;

			// create enemy in front of the player
			var pos = transform.position + transform.forward * positionOffset.z + (transform.right * positionOffset.x) * sidewaysDistribution.Evaluate(Random.Range(-1f, 1f)) + transform.up * positionOffset.y;
			var e = Instantiate(enemyPrefab, pos, Quaternion.identity) as Transform;
			StartCoroutine(pTween.To(raiseUpTime, t => {
				if (e != null) {
					e.position += Vector3.up * raiseUpSpeed * Time.deltaTime;
				}
			}));
			enemiesToKill.Add(e);
		}

		if (playerSafe != null) {
			if (!playerSafe.safe) {
				unsafeTime += Time.deltaTime / cycleDurationMultiplier;
				if (unsafeTime > maxUnsafeTime) unsafeTime = maxUnsafeTime;

			} else {
				if (unsafeTime <= 0) {
					unsafeTime = 0;
				} else {
					unsafeTime -= Time.deltaTime * coolDownMultiplier;
					//	PotatoCounter.potatoAmount -= Time.deltaTime * potatoCounter.potatoDepleteSpeed;
				}
			}
		}

		if (GetHealth() == 0) {
			if (!chaseOn) {
				chaseOn = true;
				AudioNodesManager.PostEvent("Chase", gameObject);
			}
		} else {
			if (chaseOn) {
				chaseOn = false;

			}
		}


	}
}
