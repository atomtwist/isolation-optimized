﻿using UnityEngine;
using System.Collections;

public class RandomRotateABit : MonoBehaviour
{

	public bool scale;
	public Vector3 minScale, maxScale;
	public AnimationCurve scaleDistribution;

	public bool rotation;
	public Vector3 minRotate, maxRotate;
	public AnimationCurve rotateDistribution;



	void Start()
	{
		if (scale) {
			transform.localScale = new Vector3(
				Mathf.Lerp(minScale.x, maxScale.x, scaleDistribution.Evaluate(Random.Range(0, 1f))),
				Mathf.Lerp(minScale.y, maxScale.y, scaleDistribution.Evaluate(Random.Range(0, 1f))),
				Mathf.Lerp(minScale.z, maxScale.z, scaleDistribution.Evaluate(Random.Range(0, 1f)))
				);
		}
		if (rotation) {
			transform.Rotate(
				Mathf.Lerp(minRotate.x, maxRotate.x, rotateDistribution.Evaluate(Random.Range(0, 1f))),
				Mathf.Lerp(minRotate.y, maxRotate.y, rotateDistribution.Evaluate(Random.Range(0, 1f))),
				Mathf.Lerp(minRotate.z, maxRotate.z, rotateDistribution.Evaluate(Random.Range(0, 1f)))
				);
		}
	}

}
