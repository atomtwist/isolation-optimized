﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {

	public int amountConstruction = 0;
	public Transform shelterPrefab;
	public float spawnHeight;

	void Update()
	{
		if (Input.GetButtonDown("Fire1")) {
			if (amountConstruction > 0) {
				amountConstruction--;
				var s = Instantiate(shelterPrefab, transform.position+Vector3.up*spawnHeight, transform.rotation) as Transform;
				PositionOnObject.PositionOn(s, null, Vector3.down, 100, false, Vector3.zero, false);

			}
		}

	}


}
