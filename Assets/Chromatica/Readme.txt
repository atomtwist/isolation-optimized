Thanks for buying Chromatica Studio, I hope it'll fit your needs ! Full documentation is available in the Help menu.

If you have any questions or feedback, you can contact me at :

	* E-mail: thomas.hourdel@gmail.com
	* Twitter: @Chman
	* Unity Community: http://forum.unity3d.com/members/15388-Chman

Quick Links :

	* Asset Store : https://www.assetstore.unity3d.com/en/#!/content/20743
	* Itch.io : http://chman.itch.io/chromatica-studio
	* Forum : http://forum.unity3d.com/threads/chromatica-studio-the-definitive-color-grading-suite-for-unity-easy-fast-powerful.262489/
	* Online Documentation : http://www.thomashourdel.com/chromatica/doc
