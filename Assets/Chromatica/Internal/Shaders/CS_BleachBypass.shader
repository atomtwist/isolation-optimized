﻿Shader "Hidden/Chroma/BleachBypass"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Data ("Data (RGB) + (Blend)", Vector) = (0.299, 0.587, 0.114, 1.0)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Data;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;

					float lum = dot(color, _Data.rgb);
					float3 blend = float3(lum, lum, lum);
					float L = min(1.0, max(0.0, 10.0 * (lum - 0.45)));
					float3 nc = lerp(2.0 * color * blend,
									 1.0 - 2.0 * (1.0 - blend) * (1.0 - color),
									 L);

					return float4(lerp(color, nc, _Data.w), 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
