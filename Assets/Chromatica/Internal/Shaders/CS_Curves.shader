﻿Shader "Hidden/Chroma/Curves"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_CurveTex ("Curve (RGB)", 2D) = "" {}
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				sampler2D _CurveTex;

				float4 frag(v2f_img i):COLOR
				{
					float4 color = tex2D(_MainTex, i.uv);

					// Could be done with a simple 256x1 texture using one channel per curve, but for
					// some reason it fails on dx9 (works on gl & dx11)... Here comes the ugly trick.
					float3 r = tex2D(_CurveTex, float2(color.r, 0.5 / 4.0)).rgb * float3(1.0, 0.0, 0.0);
					float3 g = tex2D(_CurveTex, float2(color.g, 1.5 / 4.0)).rgb * float3(0.0, 1.0, 0.0);
					float3 b = tex2D(_CurveTex, float2(color.b, 2.5 / 4.0)).rgb * float3(0.0, 0.0, 1.0);

					return float4(r + g + b, 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
