﻿Shader "Hidden/Chroma/PhotoFilter"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Filter ("Filter (RGB) + Density (A)", Color) = (1.0, 0.5, 0.2, 0.35)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"
				#include "CS_Globals.cginc"

				sampler2D _MainTex;
				float4 _Filter;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;

					float lum = luminance(color.rgb);
					float3 filter = _Filter.rgb;
					filter = lerp(float3(0.0, 0.0, 0.0), filter, saturate(lum * 2.0));
					filter = lerp(filter, float3(1.0, 1.0, 1.0), saturate(lum - 0.5) * 2.0);
					filter = lerp(color, filter, saturate(lum * _Filter.a));

					return float4(filter, 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
