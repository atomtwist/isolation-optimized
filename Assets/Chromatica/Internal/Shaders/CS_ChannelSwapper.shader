﻿Shader "Hidden/Chroma/ChannelSwapper"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Red ("Red Source Channel (RGB)", Vector) = (1, 0, 0, 0)
		_Green ("Green Source Channel (RGB)", Vector) = (0, 1, 0, 0)
		_Blue ("Blue Source Channel (RGB)", Vector) = (0, 0, 1, 0)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Red;
				float4 _Green;
				float4 _Blue;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;
					
					float3 red = color * _Red.rgb;
					float3 green = color * _Green.rgb;
					float3 blue = color * _Blue.rgb;

					float3 result = float3(
						red.x + red.y + red.z,
						green.x + green.y + green.z,
						blue.x + blue.y + blue.z
					);

					return float4(result, 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
