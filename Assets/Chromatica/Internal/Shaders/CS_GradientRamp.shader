﻿Shader "Hidden/Chroma/GradientRamp"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_RampTex ("Ramp (RGB)", 2D) = "white" {}
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"
				#include "CS_Globals.cginc"

				sampler2D _MainTex;
				sampler2D _RampTex;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;
					float4 ramp = tex2D(_RampTex, luminance(color).rr).rgba;
					return float4(lerp(color, ramp.rgb, ramp.a), 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
