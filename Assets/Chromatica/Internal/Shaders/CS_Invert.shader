﻿Shader "Hidden/Chroma/Invert"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Blend ("Blend (Float)", Float) = 1
		_Linear ("Is Linear (Bool)", Int) = 0
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float _Blend;
				int _Linear;

				float4 frag(v2f_img i):COLOR
				{
					// Doing some cheats in this shader to get an "expected" result
					// with linear color space instead of "correct" result. If you'd
					// rather have the correct result delete the two marked lines.

					float3 oc = tex2D(_MainTex, i.uv).rgb;
					oc = _Linear ? pow(oc, 0.454545) : oc;		// Delete this for true linear
					float3 nc = float3(1.0, 1.0, 1.0) - oc;
					nc = _Linear ? pow(nc, 2.2) : nc;			// Delete this for true linear
					return float4(lerp(oc, nc, _Blend), 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
