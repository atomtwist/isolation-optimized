﻿Shader "Hidden/Chroma/Levels"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_InputMin ("Input Black", Vector) = (0, 0, 0, 1)
		_InputMax ("Input White", Vector) = (1, 1, 1, 1)
		_InputGamma ("Input Gamma", Vector) = (1, 1, 1, 1)
		_OutputMin ("Output Black", Vector) = (0, 0, 0, 1)
		_OutputMax ("Output White", Vector) = (1, 1, 1, 1)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _InputMin;
				float4 _InputMax;
				float4 _InputGamma;
				float4 _OutputMin;
				float4 _OutputMax;

				float4 frag(v2f_img i):COLOR
				{
					float4 color = tex2D(_MainTex, i.uv);
					color = lerp(_OutputMin, _OutputMax, pow(min(max(color - _InputMin, float4(0.0, 0.0, 0.0, 0.0)) / (_InputMax - _InputMin), float4(1.0, 1.0, 1.0, 1.0)), 1.0 / _InputGamma));
					return half4(color.rgb, 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
