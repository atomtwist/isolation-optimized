﻿Shader "Hidden/Chroma/Technicolor"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Exposure ("Exposure (Float)", Range(0.0, 8.0)) = 4.0
		_Balance ("Channel Balance (RGB)", Color) = (0.85, 0.85, 0.85, 1.0)
		_Blend ("Blend (Float)", Range(0.0, 1.0)) = 0.4
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float _Exposure;
				float4 _Balance;
				float _Blend;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;

					float3 balance = 1.0 / (_Balance.rgb * _Exposure);
					float2 rmul = color.rg * balance.r;
					float2 gmul = color.rg * balance.g;
					float2 bmul = color.rb * balance.b;
	
					float rneg = dot(float2(1.05, 0.62), rmul);
					float gneg = dot(float2(0.30, 1.0), gmul);
					float bneg = dot(float2(1.0, 1.05), bmul);
	
					float3 rout = rneg.rrr + float3(0.0, 1.3, 1.0);
					float3 gout = gneg.rrr + float3(1.0, 0.0, 1.05);
					float3 bout = bneg.rrr + float3(1.6, 1.6, 0.05);
	
					float3 result = rout * gout * bout;
					return float4(lerp(color, result, _Blend), 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
