Shader "Hidden/Chroma/Grayscale"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Data ("Luminance (RGB) + Blend (A)", Vector) = (0.30, 0.59, 0.11, 1.0)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Data;

				float4 frag(v2f_img i):COLOR
				{
					float4 color = tex2D(_MainTex, i.uv);
					float lum = dot(color.rgb, _Data.rgb);
					float4 result = float4(lum, lum, lum, color.a);
					return lerp(color, result, _Data.a);
				}

			ENDCG
		}
	}

	FallBack off
}
