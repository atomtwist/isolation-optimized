﻿Shader "Hidden/Chroma/Exposure"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Exposure ("Exposure (Float)", Float) = 1.0
		_Offset ("Offset (Float)", Float) = 0.0
		_Gamma ("Gamma (Float)", Float) = 1.0
		_Brightness ("Brightness (Float)", Float) = 0.0
		_Contrast ("Contrast (RGB) + Power (W)", Vector) = (0.5, 0.5, 0.5, 0.0)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float _Exposure;
				float _Gamma;
				float _Offset;
				float _Brightness;
				float4 _Contrast;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;

					// Exposure / Offset / Gamma
					color = ((color * _Exposure) + _Offset);
					color = pow(color, _Gamma);

					// Brightness
					color *= _Brightness;

					// Contrast
					color = (color - _Contrast.rgb) * _Contrast.a + _Contrast.rgb;
					color = saturate(color);

					return float4(color, 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
