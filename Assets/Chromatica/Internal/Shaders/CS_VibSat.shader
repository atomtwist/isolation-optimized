﻿Shader "Hidden/Chroma/VibSat"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Vibrance ("Channels (RGB) + Vibrance (A)", Color) = (1.0, 1.0, 1.0, 0.0)
		_Saturation ("Saturation (Float)", Range(-1.0, 1.0)) = 0.0
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"
				#include "CS_Globals.cginc"

				sampler2D _MainTex;
				float4 _Vibrance;
				float _Saturation;

				float4 frag(v2f_img i):COLOR
				{
					float3 color = tex2D(_MainTex, i.uv).rgb;

					// Vibrance
					float3 coeff = _Vibrance.rgb * _Vibrance.a;
					float lum = luminance(color);
					float sat = max(color.r, max(color.g, color.b)) - min(color.r, min(color.g, color.b));
					color = lerp(float3(lum, lum, lum), color, (1.0 + (coeff * (1.0 - (sign(coeff) * sat)))));
					
					// Saturation
					float3 hsv = RGBtoHSV(color);
					hsv.y = saturate(hsv.y + _Saturation);
					return float4(HSVtoRGB(hsv), 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
