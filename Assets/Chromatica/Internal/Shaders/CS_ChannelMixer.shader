﻿Shader "Hidden/Chroma/ChannelMixer"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Red ("Red Channel (RGB) + Constant (A)", Vector) = (1, 0, 0, 0)
		_Green ("Green Channel (RGB) + Constant (A)", Vector) = (0, 1, 0, 0)
		_Blue ("Blue Channel (RGB) + Constant (A)", Vector) = (0, 0, 1, 0)
	}

	SubShader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }
			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest 
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Red;
				float4 _Green;
				float4 _Blue;

				float4 frag(v2f_img i):COLOR
				{
					float4 color = tex2D(_MainTex, i.uv);

					float3 result = (color.rrr * _Red)
								  + (color.ggg * _Green)
								  + (color.bbb * _Blue)
								  + float3(_Red.a, _Green.a, _Blue.a);
					
					return float4(result, 1.0);
				}

			ENDCG
		}
	}

	FallBack off
}
