Shader "Hidden/Chroma/LUT"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Contribution ("Contribution (Float)", Range(0, 1)) = 1.0

		_LookupTex ("LUT (RGB)", 2D) = "white" {}
		_LookupTexDepth ("Depth LUT (RGB)", 2D) = "white" {}
		_DepthCurve ("Depth Curve (Alpha)", 2D) = "white" {}

		_MaskOpacity ("Screen Mask Opacity (Float)", Range(0, 1)) = 1.0
		_Mask ("Screen Mask (RGB)", 2D) = "white" {}
		_MaskInvert ("Screen Mask Invert (Float)", Float) = 0.0
		_MaskTilingOffset ("Screen Mask Tiling (XY) Offset (ZW)", Vector) = (1.0, 1.0, 0.0, 0.0)
	}

	CGINCLUDE
	
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		half _Contribution;
		sampler2D _LookupTex;
		sampler2D _LookupTexDepth;
		sampler2D _DepthCurve;
		sampler2D _Mask;
		half _MaskOpacity;
		half _MaskInvert;
		half4 _MaskTilingOffset;

		sampler2D_float _CameraDepthTexture;
		float4 _CameraDepthTexture_ST;
		float4 _MainTex_TexelSize;

		struct v2f
		{
			float4 pos : SV_POSITION;
			float2 uv  : TEXCOORD0;
			float2 uv2 : TEXCOORD1;
		};

		v2f vert_dual(appdata_img v)
		{
			v2f o;
			o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			o.uv =  v.texcoord.xy;
			o.uv2 = TRANSFORM_TEX(v.texcoord, _CameraDepthTexture);
		
			#if UNITY_UV_STARTS_AT_TOP
			if (_MainTex_TexelSize.y < 0.0)
				o.uv2.y = 1.0 - o.uv2.y;
			#endif
		
			return o;
		}

		// sRGB <-> Linear from http://entropymine.com/imageworsener/srgbformula/
		// using a bit more precise values than the IEC 61966-2-1 standard
		// see http://en.wikipedia.org/wiki/SRGB for more information
		half4 sRGB(half4 color)
		{
			color.rgb = (color.rgb <= half3(0.0031308, 0.0031308, 0.0031308)) ? color.rgb * 12.9232102 : 1.055 * pow(color.rgb, 0.41666) - 0.055;
			return color;
		}

		half4 Linear(half4 color)
		{
			color.rgb = (color <= half3(0.0404482, 0.0404482, 0.0404482)) ? color.rgb / 12.9232102 : pow((color.rgb + 0.055) * 0.9478672, 2.4);
			return color;
		}
		// ...

		half4 LUTcoords(half4 color, half blue)
		{
			half2 quad1 = half2(0.0, 0.0);
			quad1.y = floor(floor(blue) * 0.125);
			quad1.x = floor(blue) - quad1.y * 8.0;

			half2 quad2 = half2(0.0, 0.0);
			quad2.y = floor(ceil(blue) * 0.125);
			quad2.x = ceil(blue) - quad2.y * 8.0;

			half c1 = 0.0009765625 + (0.123046875 * color.r);
			half c2 = 0.0009765625 + (0.123046875 * color.g);

			half4 texPos = half4(0.0, 0.0, 0.0, 0.0);
			texPos.x = quad1.x * 0.125 + c1;
			texPos.y = -(quad1.y * 0.125 + c2);
			texPos.z = quad2.x * 0.125 + c1;
			texPos.w = -(quad2.y * 0.125 + c2);

			return texPos;
		}

		half4 LUT(half4 color)
		{
			half blue = color.b * 63.0;
			half4 texPos = LUTcoords(color, blue);

			half3 finalColor = lerp(tex2D(_LookupTex, texPos.xy).rgb,
									tex2D(_LookupTex, texPos.zw).rgb,
									frac(blue));

			return lerp(color, half4(finalColor, color.a), _Contribution);
		}

		half4 DualLUT(half2 uv2, half4 color)
		{
			half blue = color.b * 63.0;
			half4 texPos = LUTcoords(color, blue);
			half fb = frac(blue);

			// Near LUT
			half3 colorNear = lerp(tex2D(_LookupTex, texPos.xy).rgb,
								   tex2D(_LookupTex, texPos.zw).rgb,
								   fb);

			// Far LUT
			half3 colorFar = lerp(tex2D(_LookupTexDepth, texPos.xy).rgb,
								  tex2D(_LookupTexDepth, texPos.zw).rgb,
								  fb);

			half d = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv2);
			half d10 = tex2D(_DepthCurve, half2(Linear01Depth(d), 0.5)).a;
			half3 finalColor = lerp(colorNear, colorFar, d10);
			return lerp(color, half4(finalColor, color.a), _Contribution);
		}

		half4 Mask(half2 uv, half4 color, half4 correctedColor)
		{
			half3 m = abs(_MaskInvert - tex2D(_Mask, uv * _MaskTilingOffset.xy + _MaskTilingOffset.zw).rgb) * _MaskOpacity;
			return half4(lerp(correctedColor.rgb, color.rgb, m), color.a);
		}

	ENDCG

	SubShader
	{
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }

		// (0) Normal - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return LUT(color);
				}

			ENDCG
		}

		// (1) Masked - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return Mask(i.uv, color, LUT(color));
				}

			ENDCG
		}

		// (2) Horizontal Split - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.x > 0.5 ? LUT(color) : color;
				}

			ENDCG
		}

		// (3) Vertical Split - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.y < 0.5 ? LUT(color) : color;
				}

			ENDCG
		}

		// (4) Normal (Dual) - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return DualLUT(i.uv2, color);
				}

			ENDCG
		}

		// (5) Masked (Dual) - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return Mask(i.uv, color, DualLUT(i.uv2, color));
				}

			ENDCG
		}

		// (6) Horizontal Split (Dual) - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.x > 0.5 ? DualLUT(i.uv2, color) : color;
				}

			ENDCG
		}

		// (7) Vertical Split (Dual) - GAMMA
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.y < 0.5 ? DualLUT(i.uv2, color) : color;
				}

			ENDCG
		}

		// --------------------------------------------------------------------------------
		//                                    LINEAR
		// --------------------------------------------------------------------------------

		// (8) Normal - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(sRGB(tex2D(_MainTex, i.uv)));
					return Linear(LUT(color));
				}

			ENDCG
		}

		// (9) Masked - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 orig = tex2D(_MainTex, i.uv);
					half4 color = saturate(sRGB(orig));
					return Mask(i.uv, orig, Linear(LUT(color)));
				}

			ENDCG
		}

		// (10) Horizontal Split - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.x > 0.5 ? Linear(LUT(sRGB(color))) : color;
				}

			ENDCG
		}

		// (11) Vertical Split - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f_img i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.y < 0.5 ? Linear(LUT(sRGB(color))) : color;
				}

			ENDCG
		}

		// (12) Normal (Dual) - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(sRGB(tex2D(_MainTex, i.uv)));
					return Linear(DualLUT(i.uv2, color));
				}

			ENDCG
		}

		// (12) Masked (Dual) - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 orig = tex2D(_MainTex, i.uv);
					half4 color = saturate(sRGB(orig));
					return Mask(i.uv, orig, Linear(DualLUT(i.uv2, color)));
				}

			ENDCG
		}

		// (13) Horizontal Split (Dual) - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.x > 0.5 ? Linear(DualLUT(i.uv2, sRGB(color))) : color;
				}

			ENDCG
		}

		// (14) Vertical Split (Dual) - LINEAR
		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_dual
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#pragma target 3.0

				half4 frag(v2f i) : COLOR
				{
					half4 color = saturate(tex2D(_MainTex, i.uv));
					return i.uv.y < 0.5 ? Linear(DualLUT(i.uv2, sRGB(color))) : color;
				}

			ENDCG
		}
	}

	FallBack off
}
