﻿Shader "Hidden/Chroma/Texture Lerp"
{
	Properties
	{
		_MainTex ("From Texture (RGB)", 2D) = "white" {}
		_To ("To Texture (RGB)", 2D) = "white" {}
		_T ("Interpolation Factor (Float)", Float) = 0.0
	}

	SubShader
	{
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }

		Pass
		{			
			CGPROGRAM

				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma exclude_renderers flash
				#pragma glsl
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				sampler2D _To;
				half _T;

				half4 frag(v2f_img i) : COLOR
				{
					half4 a = tex2D(_MainTex, i.uv);
					half4 b = tex2D(_To, i.uv);
					return lerp(a, b, _T);
				}

			ENDCG
		}
	}

	FallBack off
}
