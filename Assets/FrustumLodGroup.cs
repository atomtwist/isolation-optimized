﻿using UnityEngine;
using System.Collections;

public class FrustumLodGroup : MonoBehaviour {

	public Transform camera;
	public float angle = 60;
	public GameObject culledObj;

	void Update () {
		if (camera == null) return;
		var a = Vector3.Angle(camera.forward, transform.position - camera.position);
		if (a > angle) {
			culledObj.SetActive(false);
		} else {
			culledObj.SetActive(true);
		}
	}
}
