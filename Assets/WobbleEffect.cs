﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class WobbleEffect : MonoBehaviour
{

	public Camera[] cams;
	private Fisheye[] fishEyes = new Fisheye[2];

	public float duration;
	public float intensity;
	public AnimationCurve curve;

	void Start() {
		int i = 0;
		foreach (var c in cams) {
			fishEyes[i++] = c.GetComponent<Fisheye>();
		}
	}

	// Use this for initialization
	public void Wobble()
	{
		foreach (var f in fishEyes)	{
			f.enabled = true;

		}

		StartCoroutine(pTween.To(duration, t => {
			foreach (var f in fishEyes) {

				f.strengthX = curve.Evaluate(t) * intensity;
				f.strengthY = curve.Evaluate(t) * intensity;
				if (t==1) {
						f.enabled =false;
				}
			
			}

		}));
	}
}
