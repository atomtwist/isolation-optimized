﻿using UnityEngine;
using System.Collections;

public class WindyTrees : MonoBehaviour
{

	public Transform windDirection, windDirection2;

	public AnimationCurve curve;

	private Quaternion initRot;
	public float freq;
	float initOffset;
	public float xOffsetMultiplier = 1;
	public float magnitude;

	public float randomRotationAmount;

	Quaternion prevRotation;

	//public float lerpTime, updateSeconds;

	// Use this for initialization
	void Start()
	{
		initOffset = transform.position.x * xOffsetMultiplier + Random.Range(0, 0.2f) + 1000;
		initRot = transform.rotation;

		//StartCoroutine(UpdateInfreq());
	}

	// Update is called once per frame
	void Update()
	{
		var lerpVal = 0f;
		lerpVal = curve.Evaluate(Time.time * freq + initOffset);
		
		Quaternion newRot = Quaternion.Lerp(windDirection.rotation, windDirection2.rotation, lerpVal);
		transform.rotation *= Quaternion.Inverse(prevRotation);
		transform.rotation *= newRot;

		prevRotation = newRot;

	}

	//// Update is called once per frame
	//IEnumerator UpdateInfreq()
	//{
	//	while (true) {
	//		var lerpVal = 0f;
	//		lerpVal = curve.Evaluate(Time.time * freq + initOffset);

	//		Quaternion newRot = Quaternion.Lerp(windDirection.rotation, windDirection2.rotation, lerpVal);
	//		yield return StartCoroutine(pTween.To(lerpTime, timer => {
	//			transform.rotation = Quaternion.Lerp(transform.rotation, newRot, timer);
	//		}));
	//		//transform.rotation *= Quaternion.Inverse(prevRotation);
	//		//transform.rotation *= newRot;

	//		prevRotation = newRot;

	//		yield return new WaitForSeconds(updateSeconds);
	//	}
	//}
}
