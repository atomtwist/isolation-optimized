﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using System.Linq;

[CustomEditor(typeof(EventNode),true)]
public class EventNodeInspector : Editor {

	ReorderableList list;
	EventNode eNode;

	public float gap = 16;
	public float numberOfItems = 3;
	private void OnEnable()
	{
		list = new ReorderableList(serializedObject, serializedObject.FindProperty("eventAction"),true,true,true,true);
		list.drawHeaderCallback =  
		(Rect rect) => {
			GUI.Label(rect, "Event Actions");
		};
		list.drawElementCallback =  
		(Rect rect, int index, bool isActive, bool isFocused) => {
			//get property
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			rect = new Rect(rect.x,rect.y, rect.width,EditorGUIUtility.singleLineHeight);
			//layout shit
			DrawEventActionPopup(element, rect);
			rect.y = rect.y + gap;
			DrawPlaybackActions(element, rect);
			DrawMixerActions(element, rect);
		};
		eNode = target as EventNode;
	}


	void DrawEventActionPopup(SerializedProperty property, Rect rect)
	{
		property.FindPropertyRelative("actionType").enumValueIndex = EditorGUI.Popup(rect, "Event Action", property.FindPropertyRelative("actionType").enumValueIndex,property.FindPropertyRelative("actionType").enumDisplayNames);
	}
	
	void DrawPlaybackActions(SerializedProperty property, Rect rect)
	{
		//play
		if ( property.FindPropertyRelative("actionType").enumValueIndex == (int)EventActionType.Play)
		{
			DrawAudioNodePopup(property, rect);
		}
		//stop
		if ( property.FindPropertyRelative("actionType").enumValueIndex == (int)EventActionType.Stop)
		{
			DrawAudioNodePopup(property, rect);
			rect.y += gap;
			property.FindPropertyRelative("actionScope").enumValueIndex = EditorGUI.Popup(rect, "Action Scope", property.FindPropertyRelative("actionScope").enumValueIndex,property.FindPropertyRelative("actionScope").enumDisplayNames);
		}
		if ( property.FindPropertyRelative("actionType").enumValueIndex == (int)EventActionType.StopAll)
		{
			property.FindPropertyRelative("actionScope").enumValueIndex = EditorGUI.Popup(rect, "Action Scope", property.FindPropertyRelative("actionScope").enumValueIndex,property.FindPropertyRelative("actionScope").enumDisplayNames);
		}
	}
	
	void DrawAudioNodePopup(SerializedProperty property, Rect rect)
	{
		EditorGUI.BeginChangeCheck();
		var audioNodes = GameObject.FindObjectsOfType<AudioNode>().ToList();
		//var audioNodeID = property.FindPropertyRelative ("uniqueAudioNodeID").intValue;
		var audioNodeNames = audioNodes.Select(n => n.name).ToArray();
		var audioNodeIDList = audioNodes.Select (n => n.uniqueID).ToList();
		var selectedIndex = EditorGUI.Popup( rect, "Audio Node ",audioNodeIDList.IndexOf(property.FindPropertyRelative ("uniqueAudioNodeID").intValue), audioNodeNames);
		selectedIndex = Mathf.Clamp(selectedIndex,0,int.MaxValue);
		var selectedNode = audioNodes[selectedIndex];
		property.FindPropertyRelative("selectedNodeGameObject").objectReferenceValue = selectedNode.gameObject;
		if( EditorGUI.EndChangeCheck()) 
		{
			property.FindPropertyRelative ("uniqueAudioNodeID").intValue = selectedNode.uniqueID;
		}
	}

	void DrawMixerActions(SerializedProperty property, Rect rect)
	{
		if ( property.FindPropertyRelative("actionType").enumValueIndex == (int)EventActionType.TriggerMixerSnapshot)
		{
			EditorGUI.PropertyField(rect, property.FindPropertyRelative("mixerSnapshot"));
			rect.y += gap;
			property.FindPropertyRelative("mixerSnapshotTransitionTime").floatValue = EditorGUI.FloatField(rect, "Transition Time", property.FindPropertyRelative("mixerSnapshotTransitionTime").floatValue);
		}
	}



	void AuditionButtons()
	{
		if (GUILayout.Button("Audition Event"))
		{
			eNode.AuditionEvent();
		}
		if (GUILayout.Button("StopAudition"))
		{
			eNode.StopAudition();
		}
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		EditorGUILayout.Space();
		serializedObject.Update();
		list.elementHeight = 23.333333f * numberOfItems;
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
		EditorGUILayout.BeginVertical("Box");
		EditorGUILayout.Space();
		AuditionButtons();
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
	}
}
